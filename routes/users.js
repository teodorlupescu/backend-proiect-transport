var express = require('express');
var router = express.Router();
var sequelize = require('sequelize');
var validator = require('validator');
var bcrypt = require('bcrypt');
var jsonwebtoken = require('jsonwebtoken');

/* GET users listing. */
router.post('/register', function (req, res, next) {
	var data = req.body;
	if (!validator.isEmail(data.email) || !data.password || data.password.length < 7 || !data.name || !data.securityAnswer) {
		res.status(400).json({ results: 'Register error' });
	} else {
		return global.sequelize.sync().then(function () {
			global.usersTable.findAll().then(function (users) {
				if (users.find(function (user) {
					return user.email == data.email;
				})) {
					res.status(400).json({ results: 'Email already registered' });
					return;
				}

				return bcrypt.genSalt(function (error, salt) {
					if (error) {
						res.status(400).json({ results: 'Error' });
						return;
					}
					bcrypt.hash(data.password, salt, function (err, hash) {
						if (err) {
							res.status(400).json({ results: 'Error' });
							return;
						}

						const userdata = {
							email: data.email,
							name: data.name,
							password: hash,
							securityAnswer: data.securityAnswer
						};
						return new global.usersTable(userdata).save().then(function (user) {
							res.status(200).json({
								results: 'Success register',
								id: user.id,
								email: user.email,
								name: user.name
							})
						});
					})
				})
			})
		})
	}
});

/* GET users listing. */
router.post('/login', function (req, res, next) {
	var data = req.body;
	if (!validator.isEmail(data.email) || !data.password || data.password.length < 7) {
		return res.status(401).json({ results: 'Login error' });
	} else {
		return global.sequelize.sync().then(function () {
			global.usersTable.findOne({ where: { email: data.email } }).then(function (user) {
				bcrypt.compare(data.password, user.password, function (error, ok) {
					if (error || !ok) {
						return res.status(401).json({ results: 'Invalid credential' });
					}

					var tokendata = {
						id: user.id,
						email: user.email
					};
					var jsonwtoken = jsonwebtoken.sign(tokendata, 'jsonsecret123123', {});
					res.status(200).json({ jsonwtoken, id: user.id, email: user.email, name: user.name });
				})
			}).catch(function (error) {
				if (error) {
					return res.status(400).json({ results: 'User non existent' });
				}
			})
		})
	}
});

/* GET users listing. */
router.get('/autologin', function (req, res, next) {
	var token = req.headers.authentication;
	if (!token || typeof token !== 'string') {
		return res.status(200).json({ status: 'notloggedin', });
	}
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ message: 'error', });
		}
		return global.usersTable.findOne({ where: { id: tokendata.id } }).then(function (user) {
			if (user) {
				return res.status(200).json({
					id: user.id,
					email: user.email,
					name: user.name
				})
			}
		}).catch(function (error) {
			if (error) {
				return res.status(400).json({ results: 'User non existent' });
			}
		})
	})
});

router.put('/name', function (req, res, next) {
	var token = req.headers.authentication;
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ message: 'error', });
		}
		return global.usersTable.update({ name: req.body.name }, { where: { id: tokendata.id } }).then(function (user) {
			if (user) {
				return res.status(200).json({ name: user.name })
			}
		}).catch(function (error) {
			if (error) {
				return res.status(400).json({ results: 'User non existent' });
			}
		})
	})
});

router.delete('/deactivate', function (req, res, next) {
	var token = req.headers.authentication;
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ message: 'error', });
		}
		return global.usersTable.destroy({ where: { id: tokendata.id } }).then(function (user) {
			if (user) {
				return res.status(200).json({ results: 'success' })
			}
		}).catch(function (error) {
			if (error) {
				return res.status(400).json({ results: 'User non existent' });
			}
		})
	})
});

router.put('/password', function (req, res, next) {
	var token = req.headers.authentication;
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ message: 'error', });
		}
		return bcrypt.genSalt(function (error, salt) {
			if (error) {
				res.status(400).json({ results: 'Error' });
				return;
			}
			bcrypt.hash(req.body.password, salt, function (err, hash) {
				if (err) {
					res.status(400).json({ results: 'Error' });
					return;
				}

				return global.usersTable.update({ password: hash }, { where: { id: tokendata.id } }).then(function (user) {
					return res.status(200).json({ success: 'success' });
				}).catch(function (error) {
					if (error) {
						return res.status(400).json({ results: 'User non existent' });
					}
				})
			})
		})
	})
});

router.put('/reset-password', function (req, res, next) {
	global.usersTable.findOne({ where: { email: req.body.email } }).then(function (user) {
		if (user.securityAnswer !== req.body.securityAnswer) {
			return res.status(403).json({ results: 'Raspuns incorect', });
		}
		return bcrypt.genSalt(function (error, salt) {
			if (error) {
				res.status(400).json({ results: 'Error' });
				return;
			}
			bcrypt.hash(req.body.password, salt, function (err, hash) {
				if (err) {
					res.status(400).json({ results: 'Error' });
					return;
				}

				return global.usersTable.update({ password: hash }, { where: { email: req.body.email } }).then(function () {
					return res.status(200).json({ success: 'success' });
				}).catch(function (error) {
					if (error) {
						return res.status(400).json({ results: 'User non existent' });
					}
				})
			})
		})
	});
});

module.exports = router;
