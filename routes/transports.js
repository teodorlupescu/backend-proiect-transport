var express = require('express');
var router = express.Router();
var jsonwebtoken = require('jsonwebtoken');

/* GET users listing. */
router.post('/transport', function (req, res, next) {
	var token = req.headers.authentication;
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ results: 'error', });
		}

		var data = req.body;
		if (
			!data.leave ||
			!data.arrive ||
			!data.transportType ||
			!data.startedOn ||
			!data.duration ||
			!data.busyFactor ||
			!data.observations ||
			!data.satisfaction
		) {
			return res.status(400).json({ results: 'Date insuficiente pentru a adauga calatoria' });
		} else {
			return global.sequelize.sync().then(function () {
				const transportData = {
					addedBy: tokendata.id,
					...data
				};
				return new global.transportsTable(transportData).save().then(function (transport) {
					return global.usersTable.findOne({ where: { id: transport.addedBy } }).then(function (user) {
						res.status(200).json({
							results: 'Success register',
							transport: transport,
							user: { name: user.name }
						})
					})
				});
			})
		}
	})
});

router.get('/transport/:transportId', function (req, res, next) {
	var transportId = req.params.transportId;
	if (typeof parseInt(transportId) !== 'number') {
		return res.status(400).json({ results: 'Invalid id' })
	}
	return global.sequelize.sync().then(function () {
		return global.transportsTable.findOne({ where: { id: parseInt(transportId) } }).then(function (transport) {
			if (transport) {
				return global.usersTable.findOne({ where: { id: transport.addedBy } }).then(function (user) {
					return res.status(200).json({
						transport: transport,
						user: { name: user.name || 'Anonymous user' }
					})
				}).catch(error => {
					return res.status(200).json({
						transport: transport,
						user: { name: 'Anonymous user' }
					})
				})
			} else {
				return res.status(400).json({ results: 'Transport non existent' });
			}
		}).catch(function (error) {
			if (error) {
				return res.status(400).json({ results: 'Transport non existent' });
			}
		})
	})

});

router.get('/transports', function (req, res, next) {
	return global.transportsTable.findAll().then(function (transports) {
		var promises = [];
		transports.forEach(transport => {
			promises.push(global.usersTable.findOne({ where: { id: transport.addedBy } }).then(function (user) {
				return {
					transport: transport,
					user: { name: user.name || 'Anonymous user' }
				}
			}).catch(error => {
				return {
					transport: transport,
					user: { name: 'Anonymous user' }
				}
			}))
		});
		return Promise.all(promises).then(function (response) {
			return res.status(200).json({ transports: response })
		}).catch(function (error) {
			if (error) {
				return res.status(400).json({ results: 'A aparut o eroare neasteptata' });
			}
		})
	})
});

router.get('/transports/:userId', function (req, res, next) {
	var userId = req.params.userId;
	return global.transportsTable.findAll({ where: { addedBy: userId } }).then(function (transports) {
		var promises = [];
		transports.forEach(transport => {
			promises.push(global.usersTable.findOne({ where: { id: transport.addedBy } }).then(function (user) {
				return {
					transport: transport,
					user: { name: user.name || 'Anonymous user' }
				}
			}).catch(error => {
				return {
					transport: transport,
					user: { name: 'Anonymous user' }
				}
			}))
		});
		return Promise.all(promises).then(function (response) {
			return res.status(200).json({ transports: response })
		}).catch(function (error) {
			if (error) {
				return res.status(400).json({ results: 'A aparut o eroare neasteptata' });
			}
		})
	})
});

router.get('/my-transports', function (req, res, next) {
	var token = req.headers.authentication;
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ results: 'error', });
		}
		return global.transportsTable.findAll({ where: { addedBy: tokendata.id } }).then(function (transports) {
			var promises = [];
			transports.forEach(transport => {
				promises.push(global.usersTable.findOne({ where: { id: transport.addedBy } }).then(function (user) {
					return {
						transport: transport,
						user: { name: user.name || 'Anonymous user' }
					}
				}).catch(error => {
					return {
						transport: transport,
						user: { name: 'Anonymous user' }
					}
				}))
			});
			return Promise.all(promises).then(function (response) {
				return res.status(200).json({ transports: response })
			}).catch(function (error) {
				if (error) {
					return res.status(400).json({ results: 'A aparut o eroare neasteptata' });
				}
			})
		})
	})
});

router.put('/transport', function(req, res, next) {
	var token = req.headers.authentication;
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ results: 'error', });
		}

		var data = req.body;
		if (
			!data.leave ||
			!data.arrive ||
			!data.transportType ||
			!data.startedOn ||
			!data.duration ||
			!data.busyFactor ||
			!data.observations ||
			!data.satisfaction
		) {
			return res.status(400).json({ results: 'Date insuficiente pentru a edita calatoria' });
		} else {
			return global.transportsTable.update({ ...data }, { where: { id: parseInt(data.id), addedBy: tokendata.id } }).then(function () {
				return res.status(200).json({ success: 'success' });
			}).catch(function (error) {
				if (error) {
					return res.status(400).json({ results: 'Transport non existent' });
				}
			})
		}
	});
});

router.delete('/transport/:transportId', function(req, res, next) {
	var transportId = req.params.transportId;
	var token = req.headers.authentication;
	jsonwebtoken.verify(token, 'jsonsecret123123', function (error, tokendata) {
		if (error) {
			return res.status(401).json({ results: 'error', });
		}

		return global.transportsTable.destroy({ where: { id: transportId, addedBy: tokendata.id } }).then(function (user) {
			if (user) {
				return res.status(200).json({ results: 'success' })
			}
		}).catch(function (error) {
			if (error) {
				return res.status(400).json({ results: 'Transport non existent' });
			}
		})

	})
});

module.exports = router;