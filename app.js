
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser');
var app = express();
var sequelize = require('sequelize');
var mysql = require('mysql2/promise.js');
var dataBase;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false, limit: '12000kb' }));
app.use(bodyParser.json({ extended: false, limit: '12000kb' }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var transportsRouter = require('./routes/transports');

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/transports', transportsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

mysql.createConnection({
  user: 'utilizator',
  password: 'parolamea123'
}).then(function(database) {
  dataBase = database;
  global.database = database;
  return database.query(`CREATE DATABASE IF NOT EXISTS transport`).then(function() {
    global.sequelize = new sequelize('transport', 'utilizator', 'parolamea123', { logging: false, dialect: 'mysql', });
    console.log('OK');
    global.usersTable = global.sequelize.define('users', {
      name: sequelize.STRING,
      email: sequelize.STRING,
      password: sequelize.STRING,
      securityAnswer: sequelize.STRING
    }, {
      timestamps: false,
      tableName: 'users',
    });

    global.transportsTable = global.sequelize.define('transports', {
      addedBy: sequelize.STRING,
      leave: sequelize.STRING,
      arrive: sequelize.STRING,
      transportType: sequelize.STRING,
      startedOn: sequelize.STRING,
      duration: sequelize.STRING,
      busyFactor: sequelize.STRING,
      observations: sequelize.STRING,
      satisfaction: sequelize.STRING
    }, {
      timestamps: false,
      tableName: 'transports',
    });

    dataBase.end();
  })
}).catch(function(error) {
  console.log('ERROR', JSON.stringify(error))
});

module.exports = app;
